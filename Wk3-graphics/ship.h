#ifndef _SHIP_H
#define _SHIP_H
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace shipNS
{
	const int	WIDTH = 32;		// width of the ship image
	const int	HEIGHT = 32;		// height of the ship image
	const int	X = GAME_WIDTH/2 - WIDTH/2;
	const int	Y = GAME_HEIGHT/2 - HEIGHT/2;

	const float SPEED = 100.0f; 
	const float MASS = 300.0f;
	const float ROTATION_RATE = (float)PI;
	enum DIRECTION {NONE, LEFT, RIGHT};     // rotation direction

	const int	TEXTURE_COLS = 8;		// ship texture has two 2 columns
	const int	SHIP1_START_FRAME = 0;		// starting frame of animation for ship 1
	const int	SHIP1_END_FRAME = 3;		// ending frame of animation for ship 1 (0,1,2,3)
	const int	SHIP2_START_FRAME = 8;		// starting frame of animation for ship 2
	const int	SHIP2_END_FRAME = 11;		// ending frame of animation for ship 2 (8,9,10,11)

	const float SHIP_ANIMATION_DELAY = 0.2f; // time between frames of ship animation

	const int   SHIELD_START_FRAME = 24;
	const int	SHIELD_END_FRAME = 27;		// frames 24, 25, 26, 27
	const float SHIELD_ANIMATION_DELAY = 0.3f; // time between frames of shield animation

}

class Ship : public Entity
{
private:
	bool	shieldOn;	// store if the shield is on
	bool	engineOn;
	Image	shield;		// 
	shipNS::DIRECTION direction;
	float   rotation;               // current rotation rate (radians/second)
	float	oldX;
	float	oldY;
	float	oldAngle;

public:
	Ship(); // constructor

	void update( float frameTime ); 

	virtual void draw();
	virtual bool initialize( Game *gamePtr, int width, int height, int ncols, TextureManager *texturePtr);

	void damage( WEAPON );

	// direction of rotation force
    void rotate(shipNS::DIRECTION dir) {direction = dir;}

	    // Returns rotation
    float getRotation() {return rotation;}

    // Returns engineOn condition
    bool getEngineOn()  {return engineOn;}

    // Returns shieldOn condition
    bool getShieldOn()  {return shieldOn;}

    // Sets engine on
    void setEngineOn(bool eng)  {engineOn = eng;}

    // Set shield on
    void setShieldOn(bool sh)   {shieldOn = sh;}

    // Sets Mass
    void setMass(float m)       {mass = m;}
};

#endif