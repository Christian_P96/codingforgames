

#ifndef _SPACEWAR_H
#define _SPACEWAR_H
#define WIN32_LEAN_AND_MEAN

#include "game.h"
#include "textureManager.h"
#include "image.h"
#include "ship.h"
#include "planet.h"

class Spacewar : public Game
{
private:
	// variables
//	TextureManager nebulaTexture; // nebula texture
//	TextureManager planetTexture; // planet texture 
//	TextureManager shipTexture;
	TextureManager bgTexture; 
	TextureManager gameTextures;

//	Image nebula;		// nebula image 
	Image space;		
	Planet planet;		// planet image
	Ship ship1;			// ship 1
	Ship ship2;			// ship 2

public:
	Spacewar();  // constructor
	virtual ~Spacewar(); // destructor

	void initialize( HWND hwnd );

	void update();			// override pure virtual from Game
	void ai();				// override pure virtual from Game
	void collisions();		// override pure virtual from Game
	void render();			// override pure virtual from Game

	void releaseAll();
	void resetAll();
};
#endif
