#include "ship.h"

// constructor
Ship::Ship() : Entity()
{
	spriteData.width = shipNS::WIDTH;
	spriteData.height = shipNS::HEIGHT;
	spriteData.x = shipNS::X;
	spriteData.y = shipNS::Y;
	spriteData.rect.bottom = shipNS::HEIGHT;
	spriteData.rect.right = shipNS::WIDTH;

	frameDelay = shipNS::SHIP_ANIMATION_DELAY;
	startFrame = shipNS::SHIP1_START_FRAME;
	endFrame = shipNS::SHIP1_END_FRAME;
	currentFrame = startFrame;

	collisionType = entityNS::CIRCLE;
	radius = shipNS::WIDTH/2.0f;

	velocity.x = 0;
	velocity.y = 0;
	rotation = 0.0f;

	oldX = shipNS::X;
    oldY = shipNS::Y;
    oldAngle = 0.0f;
	mass = shipNS::MASS;

	engineOn = false;
    shieldOn = false;
}

void Ship::update ( float frameTime )
{
	//Entity::update( frameTime );

	//// rotation & position
	//spriteData.angle += frameTime * shipNS::ROTATION_RATE;
	//spriteData.x += frameTime * velocity.x;
	//spriteData.y += frameTime * velocity.y; 

	//// bounce off the walls of the screen!
	//if( spriteData.x > (GAME_WIDTH - shipNS::WIDTH * getScale() ))
	//{
	//	// push it back onto the screen if it's gone past the edge
	//	spriteData.x = GAME_WIDTH - shipNS::WIDTH * getScale(); 
	//	velocity.x = -velocity.x;		//reverse direction
	//}
	//else if ( spriteData.x < 0 )
	//{
	//	spriteData.x = 0;
	//	velocity.x = -velocity.x;		//reverse direction
	//}

	//if( spriteData.y > (GAME_HEIGHT - shipNS::HEIGHT * getScale() ))
	//{
	//	// push it back onto the screen if it's gone past the edge
	//	spriteData.y = GAME_HEIGHT - shipNS::HEIGHT * getScale(); 
	//	velocity.y = -velocity.y;		//reverse direction
	//}
	//else if ( spriteData.y < 0 )
	//{
	//	spriteData.y = 0;
	//	velocity.y = -velocity.y;		//reverse direction
	//}

	if( shieldOn )
	{
		shield.update( frameTime );
		if( shield.getAnimationComplete() )
		{
			shieldOn = false; 
			shield.setAnimationComplete( false ); // reset animation
			shield.setCurrentFrame( shipNS::SHIELD_START_FRAME );
		}
	}
	if(engineOn)
    {
        velocity.x += (float)cos(spriteData.angle) * shipNS::SPEED * frameTime;
        velocity.y += (float)sin(spriteData.angle) * shipNS::SPEED * frameTime;
  
    }

    Entity::update(frameTime);
    oldX = spriteData.x;                        // save current position
    oldY = spriteData.y;
    oldAngle = spriteData.angle;

    switch (direction)                          // rotate ship
    {
		case shipNS::LEFT:
			rotation -= frameTime * shipNS::ROTATION_RATE;  // rotate left
			break;
		case shipNS::RIGHT:
			rotation += frameTime * shipNS::ROTATION_RATE;  // rotate right
			break;
    }
    spriteData.angle += frameTime * rotation;   // apply rotation


}

bool Ship::initialize( Game* gamePtr, int width, int height, int ncols, TextureManager* texturePtr )
{
	shield.initialize( gamePtr->getGraphics(), width, height, ncols, texturePtr );
	shield.setFrames( shipNS::SHIELD_START_FRAME, shipNS::SHIELD_END_FRAME );
	shield.setCurrentFrame( shipNS::SHIELD_START_FRAME );
	shield.setFrameDelay( shipNS::SHIELD_ANIMATION_DELAY );
	shield.setLoop(false); // don't want to loop the animation
	return( Entity::initialize( gamePtr, width, height, ncols, texturePtr ) );
}

void Ship::draw()
{
	Image::draw();  // draw the ship first

	if( shieldOn ) // if the shield is on, draw it on top of the ship
	{
		shield.draw( spriteData, graphicsNS::ALPHA50 & colorFilter );
		// ship's spriteData -- shield will have the same size and rotation as the ship
	}
}

void Ship::damage( WEAPON weapon )
{
	shieldOn = true;
	
}